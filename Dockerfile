FROM php:7.4-fpm

RUN apt-get update -y
RUN apk --update --no-cache add ca-certificates nginx

RUN install-php-extensions fpm mcrypt soap openssl gmp pdo_odbc json dom pdo zip mysqli sqlite3 apcu pdo_pgsql bcmath gd odbc pdo_mysql pdo_sqlite gettext xmlreader xmlrpc bz2 iconv pdo_dblib curl ctype phar fileinfo mbstring tokenizer

USER container
ENV  USER container
ENV HOME /home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/ash", "/entrypoint.sh"]